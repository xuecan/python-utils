# Copyright (C) 2012-2015 Xue Can <xuecan@gmail.com> and contributors.
# Licensed under the MIT license: http://opensource.org/licenses/mit-license

"""
# JSONify tools for Flask

Requirement:

* Flask
* simplejson
* pymongo (option)
"""

from functools import partial
import simplejson as json
from flask import current_app


def _convert_unicode_to_utf8(other_default_parser, obj):
    if isinstance(obj, unicode):
        return obj.encode("utf8")
    if other_default_parser:
        return other_default_parser(obj)
    raise TypeError(str(type(obj)))


def jsonify(status=200, default=None, **kwargs):
    """when DEBUG is True, make result of jsonify() more human friendly,
    otherwise keep JSON minified"""
    extra = dict(default=default)
    if current_app.config.get("DEBUG"):
        extra["default"] = partial(_convert_unicode_to_utf8, default)
        extra.update(dict(ensure_ascii=False, indent="    ", sort_keys=True))
    content = json.dumps(dict(**kwargs), **extra)
    resp = current_app.make_response((content, status))
    resp.mimetype = "application/json; charset=utf-8"
    return resp


def bsonify(status=200, **kwargs):
    """jsonify() with mongodb extended features in strict mode

    see also: http://docs.mongodb.org/manual/reference/mongodb-extended-json/
    """
    from bson import json_util
    return jsonify(status, json_util.default, **kwargs)
