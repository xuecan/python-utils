# -*- coding: utf-8 -*-
# Copyright (C) 2014-2015 Xue Can <xuecan@gmail.com> and contributors.
# Licensed under the MIT license: http://opensource.org/licenses/mit-license

"""
# spawn 工具
"""

import os
import subprocess


def no_wait(args):
    # http://stackoverflow.com/questions/17937249/fire-and-forget-a-process-from-a-python-script
    pid = subprocess.Popen(args, preexec_fn=os.setsid)
    return pid
