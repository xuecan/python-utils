# Python Utilities

这个仓库中 Python 包/模块在日常开发中经常使用，且并不适宜发布到 PyPI 上。

主要的原因有：

* 许多模块都非常小，最初的目的只是为了少打字。
* 有些时候可以简单的注释或取消注释几行代码让程序具有不同的行为，并没有提供参数。
* 包含了一些可用但不那么清晰的补丁，增强特定场景下第三方库应有但没有的特性。
* 经过实践检验，所以没有提供单元测试。
* 文档不够完善，鼓励多看代码。

因此，鼓励直接下载本仓库，放置到需要使用的工程中，并进行必要的修改。
