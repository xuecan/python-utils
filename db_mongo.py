# Copyright (C) 2014-2015 Xue Can <xuecan@gmail.com> and contributors.
# Licensed under the MIT license: http://opensource.org/licenses/mit-license

"""
# Tools for MongoDB

Requirement:

* pymongo
"""

from urlparse import urlparse
from bson.objectid import ObjectId
from bson.dbref import DBRef
from bson.son import SON
import pymongo


ASC = pymongo.ASCENDING
DESC = pymongo.DESCENDING

DuplicateError = pymongo.errors.DuplicateKeyError
MongoError = pymongo.errors.PyMongoError


class MagicDictMixin:
    """mixin for dict-like objects, make keys act as attributes"""

    def __getattr__(self, name):
        # unlike __setattr__ or __delattr__, __getattr__ only been
        # invoked when name is not found, so dealing with __dict__
        # is unnecessary
        return self.get(name, None)

    def __setattr__(self, name, value):
        if name in self.__dict__:
            self.__dict__[name] = value
        else:
            self[name] = value

    def __delattr__(self, name):
        if name in self:
            self.pop(name)
        else:
            dict.__delattr__(self, name)


class MagicDict(dict, MagicDictMixin):
    """dict with MagicDictMixin"""


class MagicSON(SON, MagicDictMixin):
    """bson.son.SON with MagicDictMixin"""


def make_mongodb_client(uri):
    """create MongoDB client instance"""
    info = pymongo.uri_parser.parse_uri(uri)
    # If `/database` is not specified and the connection string includes
    # credentials, the driver will authenticate to the admin database.
    # see: http://docs.mongodb.org/manual/reference/connection-string/
    database = info['database']
    if not database:
        raise ValueError('database not found')
    # keys in options are lowercased
    if 'replicaset' in info['options']:
        Client = pymongo.MongoReplicaSetClient
    else:
        Client = pymongo.MongoClient
    # set tz_aware=True can force application code to handle timezones properly
    # see: http://api.mongodb.org/python/current/faq.html#what-is-the-correct-way-to-handle-time-zones-with-pymongo
    client = Client(uri, tz_aware=True)
    client.document_class = MagicDict
    database = client[database]
    return database
