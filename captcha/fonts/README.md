# 用于 CAPTCHA 的字库

这里包含的字体有 Free 和 Free for Personal Use
两类，根据具体场景决定使用哪些字体。如果需要，请购买字体的商用授权许可。

## A Song For Jennifer

设计师： []()
授权许可： Free for Personal Use


## Boycott

设计师： [Flat-it](http://www.flat-it.com)
授权许可： Free


## Roman Antique

设计师： [Dieter Steffmann](http://moorstation.org/typoasis/designers/steffmann/index.htm)
授权许可： Free
