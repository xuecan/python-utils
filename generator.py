# -*- coding: utf-8 -*-
# Copyright (C) 2012 Xue Can <xuecan@gmail.com> and contributors.
# Licensed under the MIT license: http://www.opensource.org/licenses/mit-license

"""
# 密码生成器
"""

import uuid
import random


__password_characters__ = (
    'abcdefghizklmnopqrstuvwxyz' 'abcdefghizklmnopqrstuvwxyz'
    'ABCDEFGHIZKLMNOPQRSTUVWXYZ' 'ABCDEFGHIZKLMNOPQRSTUVWXYZ'
    '0123456789' '0123456789' '0123456789' '0123456789' '0123456789'
    '!@#$^&*()[]{}:;,.?+-=`~|'
) # 字母、数字出现的频率是不一样的，让生成的密码看起来人性化一些，避免使用 % ' " \


def generate_password(length=16):
    """密码生成器，生成的密码可能包含大小写字母、数字和符号，较为安全"""
    password = ''
    max_index = len(__password_characters__)-1
    for i in range(length):
        password += __password_characters__[random.randint(0, max_index)]
    return password


__token_characters__ = (
    'ABCDEFGHIZKLMNOPQRSTUVWXYZ' '0123456789'
)

def generate_token(prefix='', length=16):
    """令牌或标识生成器"""
    token = ''
    max_index = len(__token_characters__)-1
    for i in range(length):
        token += __token_characters__[random.randint(0, max_index)]
    return prefix + token


__shortcut_characters__ = (
    'abcdefghizklmnopqrstuvwxyz' '0123456789'
)

def generate_shortcut(prefix='', length=8):
    """快捷 id 生成器"""
    shortcut = ''
    max_index = len(__shortcut_characters__)-1
    for i in range(length):
        shortcut += __shortcut_characters__[random.randint(0, max_index)]
    return prefix + shortcut


__inputable_characters__ = (
    '0123456789' 'abcdefghizklmnopqrstuvwxyz' 'ABCDEFGHIZKLMNOPQRSTUVWXYZ'
    '!@#$%^&*()+-=_~{}[];:,./<>?' "'" '`"' '\\' ' '
)
