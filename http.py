# Copyright (C) 2012-2015 Xue Can <xuecan@gmail.com> and contributors.
# Licensed under the MIT license: http://opensource.org/licenses/mit-license

"""
simple wrappers for python-requests
===================================

Requrement:

* Requests
"""


__all__ = ["request", "logger",
           "get", "options", "head", "post", "put", "patch", "delete"]

import logging
import httplib
import requests
import os
from requests import codes
from requests.exceptions import RequestException

logger = logging.getLogger("http")
logger.propagate = True

USER_AGENT = "HTTP Agent/15.01 " + requests.utils.default_user_agent()


def request(method, url, **kwargs):
    """Constructs and sends a :class:`Request <Request>`.
    Returns :class:`Response <Response>` object.

    :param method: method for the new :class:`Request` object.
    :param url: URL for the new :class:`Request` object.
    :param params: (optional) Dictionary or bytes to be sent in the query string for the :class:`Request`.
    :param data: (optional) Dictionary, bytes, or file-like object to send in the body of the :class:`Request`.
    :param headers: (optional) Dictionary of HTTP Headers to send with the :class:`Request`.
    :param cookies: (optional) Dict or CookieJar object to send with the :class:`Request`.
    :param files: (optional) Dictionary of 'name': file-like-objects (or {'name': ('filename', fileobj)}) for multipart encoding upload.
    :param auth: (optional) Auth tuple to enable Basic/Digest/Custom HTTP Auth.
    :param timeout: (optional) Float describing the timeout of the request.
    :param allow_redirects: (optional) Boolean. Set to True if POST/PUT/DELETE redirect following is allowed.
    :param proxies: (optional) Dictionary mapping protocol to the URL of the proxy.
    :param verify: (optional) if ``True``, the SSL cert will be verified. A CA_BUNDLE path can also be provided.
    :param stream: (optional) if ``False``, the response content will be immediately downloaded.
    :param cert: (optional) if String, path to ssl client cert file (.pem). If Tuple, ('cert', 'key') pair.

    Usage::

      >>> from utils import http
      >>> req = http.request('GET', 'http://httpbin.org/get')
      <Response [200]>
    """
    headers = kwargs.setdefault("headers", dict())
    headers["User-Agent"] = USER_AGENT
    kwargs.setdefault("timeout", 5.0)
    resp = requests.request(method=method, url=url, **kwargs)
    if httplib.HTTPConnection.debuglevel > 1:
        logger.debug(repr(resp.content))
    return resp


def get(url, **kwargs):
    """Sends a GET request. Returns :class:`Response` object.

    :param url: URL for the new :class:`Request` object.
    :param \*\*kwargs: Optional arguments that ``request`` takes.
    """
    kwargs.setdefault('allow_redirects', True)
    return request('get', url, **kwargs)


def options(url, **kwargs):
    """Sends a OPTIONS request. Returns :class:`Response` object.

    :param url: URL for the new :class:`Request` object.
    :param \*\*kwargs: Optional arguments that ``request`` takes.
    """
    kwargs.setdefault('allow_redirects', True)
    return request('options', url, **kwargs)


def head(url, **kwargs):
    """Sends a HEAD request. Returns :class:`Response` object.

    :param url: URL for the new :class:`Request` object.
    :param \*\*kwargs: Optional arguments that ``request`` takes.
    """
    kwargs.setdefault('allow_redirects', False)
    return request('head', url, **kwargs)


def post(url, data=None, **kwargs):
    """Sends a POST request. Returns :class:`Response` object.

    :param url: URL for the new :class:`Request` object.
    :param data: (optional) Dictionary, bytes, or file-like object to send in the body of the :class:`Request`.
    :param \*\*kwargs: Optional arguments that ``request`` takes.
    """
    return request('post', url, data=data, **kwargs)


def put(url, data=None, **kwargs):
    """Sends a PUT request. Returns :class:`Response` object.

    :param url: URL for the new :class:`Request` object.
    :param data: (optional) Dictionary, bytes, or file-like object to send in the body of the :class:`Request`.
    :param \*\*kwargs: Optional arguments that ``request`` takes.
    """

    return request('put', url, data=data, **kwargs)


def patch(url, data=None, **kwargs):
    """Sends a PATCH request. Returns :class:`Response` object.

    :param url: URL for the new :class:`Request` object.
    :param data: (optional) Dictionary, bytes, or file-like object to send in the body of the :class:`Request`.
    :param \*\*kwargs: Optional arguments that ``request`` takes.
    """
    return request('patch', url,  data=data, **kwargs)


def delete(url, **kwargs):
    """Sends a DELETE request. Returns :class:`Response` object.

    :param url: URL for the new :class:`Request` object.
    :param \*\*kwargs: Optional arguments that ``request`` takes.
    """
    return request('delete', url, **kwargs)


# ======================================================================
# pathces to httplib, for more awesome debuging
# ======================================================================

# debuglevel > 1 will also logged response body
httplib.HTTPConnection.debuglevel = 2
# now we can use logger.setLevel(...) to setup verbosity option
# instead of using print.



# symbols used in patch methods
from httplib import (_MAXLINE, LineTooLong, BadStatusLine, LineAndFileWrapper,
                     CONTINUE, UnknownProtocol, HTTPMessage, StringIO,
                     NO_CONTENT, NOT_MODIFIED, IncompleteRead)


def _HTTPResponse__read_status(self):
    # Initialize with Simple-Response defaults
    line = self.fp.readline(_MAXLINE + 1)
    if len(line) > _MAXLINE:
        raise LineTooLong("header line")
    if self.debuglevel > 0:
        logger.debug("[RESPONSE] " + line.strip())
    if not line:
        # Presumably, the server closed the connection before
        # sending a valid response.
        raise BadStatusLine(line)
    try:
        [version, status, reason] = line.split(None, 2)
    except ValueError:
        try:
            [version, status] = line.split(None, 1)
            reason = ""
        except ValueError:
            # empty version will cause next test to fail and status
            # will be treated as 0.9 response.
            version = ""
    if not version.startswith('HTTP/'):
        if self.strict:
            self.close()
            raise BadStatusLine(line)
        else:
            # assume it's a Simple-Response from an 0.9 server
            self.fp = LineAndFileWrapper(line, self.fp)
            return "HTTP/0.9", 200, ""

    # The status code is a three-digit number
    try:
        status = int(status)
        if status < 100 or status > 999:
            raise BadStatusLine(line)
    except ValueError:
        raise BadStatusLine(line)
    return version, status, reason


def _HTTPResponse_begin(self):
    if self.msg is not None:
        # we've already started reading the response
        return

    # read until we get a non-100 response
    while True:
        version, status, reason = self._read_status()
        if status != CONTINUE:
            break
        # skip the header from the 100 response
        while True:
            skip = self.fp.readline(_MAXLINE + 1)
            if len(skip) > _MAXLINE:
                raise LineTooLong("header line")
            skip = skip.strip()
            if not skip:
                break
            if self.debuglevel > 0:
                logger.debug(skip)

    self.status = status
    self.reason = reason.strip()
    if version == 'HTTP/1.0':
        self.version = 10
    elif version.startswith('HTTP/1.'):
        self.version = 11   # use HTTP/1.1 code for HTTP/1.x where x>=1
    elif version == 'HTTP/0.9':
        self.version = 9
    else:
        raise UnknownProtocol(version)

    if self.version == 9:
        self.length = None
        self.chunked = 0
        self.will_close = 1
        self.msg = HTTPMessage(StringIO())
        return

    self.msg = HTTPMessage(self.fp, 0)
    if self.debuglevel > 0:
        logger.debug("HEADERS:\n" + "".join(self.msg.headers).strip())

    # don't let the msg keep an fp
    self.msg.fp = None

    # are we using the chunked-style of transfer encoding?
    tr_enc = self.msg.getheader('transfer-encoding')
    if tr_enc and tr_enc.lower() == "chunked":
        self.chunked = 1
        self.chunk_left = None
    else:
        self.chunked = 0

    # will the connection close at the end of the response?
    self.will_close = self._check_close()

    # do we have a Content-Length?
    # NOTE: RFC 2616, S4.4, #3 says we ignore this if tr_enc is "chunked"
    length = self.msg.getheader('content-length')
    if length and not self.chunked:
        try:
            self.length = int(length)
        except ValueError:
            self.length = None
        else:
            if self.length < 0:  # ignore nonsensical negative lengths
                self.length = None
    else:
        self.length = None

    # does the body have a fixed length? (of zero)
    if (status == NO_CONTENT or status == NOT_MODIFIED or
        100 <= status < 200 or      # 1xx codes
        self._method == 'HEAD'):
        self.length = 0

    # if the connection remains open, and we aren't using chunked, and
    # a content-length was not provided, then assume that the connection
    # WILL close.
    if not self.will_close and \
       not self.chunked and \
       self.length is None:
        self.will_close = 1


def _HTTPResponse_read(self, amt=None):
    if self.fp is None:
        return ''

    if self._method == 'HEAD':
        self.close()
        return ''

    if self.chunked:
        return self._read_chunked(amt)

    if amt is None:
        # unbounded read
        if self.length is None:
            s = self.fp.read()
        else:
            try:
                s = self._safe_read(self.length)
            except IncompleteRead:
                self.close()
                raise
            self.length = 0
        self.close()        # we read everything
        #if self.debuglevel > 1:
        #    logger.debug("CONTENT:\n" + repr(s))
        return s

    if self.length is not None:
        if amt > self.length:
            # clip the read to the "end of response"
            amt = self.length

    # we do not use _safe_read() here because this may be a .will_close
    # connection, and the user is reading more bytes than will be provided
    # (for example, reading in 1k chunks)
    s = self.fp.read(amt)
    if not s:
        # Ideally, we would raise IncompleteRead if the content-length
        # wasn't satisfied, but it might break compatibility.
        self.close()
    if self.length is not None:
        self.length -= len(s)
        if not self.length:
            self.close()
    #if self.debuglevel > 1:
    #    logger.debug("CONTENT:\n" + repr(s))
    return s


def _HTTPConnection_send(self, data):
    """Send `data' to the server."""
    if self.sock is None:
        if self.auto_open:
            self.connect()
        else:
            raise NotConnected()

    if self.debuglevel > 0:
        logger.debug(">"*8 + "\n" + data.strip())
    blocksize = 8192
    if hasattr(data, 'read') and not isinstance(data, array):
        if self.debuglevel > 0:
            logger.debug("sendIng a read()able")
        datablock = data.read(blocksize)
        while datablock:
            self.sock.sendall(datablock)
            datablock = data.read(blocksize)
    else:
        self.sock.sendall(data)


def _HTTPConnection__set_content_length(self, body):
    # Set the content-length based on the body.
    thelen = None
    try:
        thelen = str(len(body))
    except TypeError, te:
        # If this is a file-like object, try to
        # fstat its file descriptor
        try:
            thelen = str(os.fstat(body.fileno()).st_size)
        except (AttributeError, OSError):
            # Don't send a length if this failed
            if self.debuglevel > 0:
                logger.debug("Cannot stat!!")

    if thelen is not None:
        self.putheader('Content-Length', thelen)


# apply patches
httplib.HTTPResponse._read_status = _HTTPResponse__read_status
httplib.HTTPResponse.begin = _HTTPResponse_begin
httplib.HTTPResponse.read = _HTTPResponse_read
httplib.HTTPConnection.send = _HTTPConnection_send
httplib.HTTPConnection._set_content_length = _HTTPConnection__set_content_length
