# -*- coding: utf-8 -*-
# Copyright (C) 2012-2015 Xue Can <xuecan@gmail.com> and contributors.
# Licensed under the MIT license: http://opensource.org/licenses/mit-license

import sys


def ensure_unicode(data):
    """str->unicode，通常用于日志信息的处理，确保一定会成功并尽可能返回更多有意义的信息"""
    if not isinstance(data, basestring):
        data = str(data)
    if isinstance(data, unicode):
        return data
    # 尝试1：直接使用 unicode()
    try:
        data = unicode(data)
        return data
    except:
        pass
    # 尝试2：使用文件系统编码解码，例如 Windows 下非英文路径
    try:
        data = data.decode(sys.getfilesystemencoding())
        return data
    except:
        pass
    # 尝试3：使用 UTF-8 解码
    try:
        data = data.decode('utf8')
        return data
    except:
        pass
    # 到这里还没有触发 return，说明整体处理失败了，启动逐行处理
    # 只有一行，返回解码错误信息
    lines = data.splitlines()
    if len(lines) <= 1:
        # 只有 1 行
        return u'<DECODE ERROR>'
    # 有多行，尝试逐行解码，这样可以尽可能少的丢失信息
    result = [ensure_unicode(line) for line in lines]
    return u'\n'.join(result)


def boolean(data):
    """三态(True, False, None)布尔值，支持不同的字符串形式"""
    if type(data) in [bool, type(None)]:
        return data
    elif isinstance(data, basestring):
        data = data.lower()
        if data in ["true", "t", "yes", "y", "1", "on", u"是"]:
            return True
        if data in ["false", "f", "no", "n", "0", "off", u"否"]:
            return False
        elif data in ["", "null", "nil", "none", u"空"]:
            return None
        else:
            return True
    else:
        return bool(data)
