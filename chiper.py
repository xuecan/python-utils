# Copyright (C) 2013-2015 Xue Can <xuecan@gmail.com> and contributors.
# Licensed under the MIT license: http://opensource.org/licenses/mit-license

"""
# 3DES and AES Ciphers

Requirement:

* pycrypto
"""

from base64 import urlsafe_b64encode, urlsafe_b64decode
from Crypto.Cipher import DES3
from Crypto.Cipher import AES
from Crypto import Random


def urlsafe_encode(data):
    return urlsafe_b64encode(data)


def urlsafe_decode(data):
    return urlsafe_b64decode(data)


def append_padding(block_size, data):
    # length of padding
    padding_length = block_size - len(data) % block_size
    # PKCS5 padding content
    padding = chr(padding_length) * padding_length
    return data + padding


def remove_padding(data):
    padding_length = ord(data[-1])
    return data[:-padding_length]


def des3_encrypt(key, iv, data=None):
    """
    des3_encrypt(key, data)
    des3_encrypt(key, iv, data)
    """
    if data is None:
        data = iv
        iv = Random.new().read(DES3.block_size)
        random_iv = True
    else:
        random_iv = False
    encryptor = DES3.new(key, DES3.MODE_CBC, iv)
    data = append_padding(DES3.block_size, data)
    encrypted = encryptor.encrypt(data)
    if random_iv:
        return iv + encrypted
    else:
        return encrypted


def des3_decrypt(key, iv, data=None):
    """
    des3_decrypt(key, data)
    des3_decrypt(key, iv, data)
    """
    if data is None:
        data = iv[DES3.block_size:]
        iv = iv[:DES3.block_size]
    encryptor = DES3.new(key, DES3.MODE_CBC, iv)
    decrypted = encryptor.decrypt(data)
    decrypted = remove_padding(decrypted)
    return decrypted


def aes_encrypt(key, iv, data=None):
    """
    aes_encrypt(key, data)
    aes_encrypt(key, iv, data)
    """
    if data is None:
        data = iv
        iv = Random.new().read(AES.block_size)
        random_iv = True
    else:
        random_iv = False
    encryptor = AES.new(key, AES.MODE_CBC, iv)
    data = append_padding(AES.block_size, data)
    encrypted = encryptor.encrypt(data)
    if random_iv:
        return iv + encrypted
    else:
        return encrypted


def aes_decrypt(key, iv, data=None):
    """
    aes_decrypt(key, data)
    aes_decrypt(key, iv, data)
    """
    if data is None:
        data = iv[AES.block_size:]
        iv = iv[:AES.block_size]
    encryptor = AES.new(key, AES.MODE_CBC, iv)
    decrypted = encryptor.decrypt(data)
    decrypted = remove_padding(decrypted)
    return decrypted
