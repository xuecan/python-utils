# Copyright (C) 2014-2015 Xue Can <xuecan@gmail.com> and contributors.
# Licensed under the MIT license: http://opensource.org/licenses/mit-license

"""
# Tools for PostgreSQL with SQLAlchemy

Requirement:

* SQLAlchemy
* psycopg2
"""

from sqlalchemy import create_engine


def make_engine(uri, debug=False):
    # client_encoding="utf8"
    # http://docs.sqlalchemy.org/en/rel_0_9/dialects/postgresql.html#unicode-with-psycopg2

    # paramstyle="format"
    # http://docs.sqlalchemy.org/en/rel_0_9/dialects/postgresql.html#bound-parameter-styles

    # isolation_level="READ COMMITTED"
    # http://docs.sqlalchemy.org/en/rel_0_9/dialects/postgresql.html#postgresql-isolation-level

    # In PostgreSQL, there are only three distinct isolation levels,
    # which correspond to the levels READ COMMITTED, REPEATABLE READ, and SERIALIZABLE.
    # http://www.postgresql.org/docs/9.4/static/transaction-iso.html
    if (not uri.startswith("postgresql://")) \
        and (not uri.startswith("postgresql+psycopg2://")):
        raise ValueError("not for psycopg2")
    engine = create_engine(
        uri,
        client_encoding="utf8",
        paramstyle="format",
        isolation_level="READ COMMITTED",
        echo=debug
    )
    return engine
