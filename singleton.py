# -*- coding: utf-8 -*-
# Copyright (C) 2012 Xue Can <xuecan@gmail.com> and contributors.
# Licensed under the MIT license: http://opensource.org/licenses/mit-license


class singleton(type):
    """用于实现 singleton 设计模式的 metaclass

    需要运用 singleton 设计模式的类，只需要该类或其祖先设置类属性 __metaclass__
    为本类即可：

        class A(object):
            __metaclass__ = singleton

        class B(A):
            pass

    在上面的例子中，类 A 和 B 都是 singleton 的。
    """

    def __init__(cls, name, bases, dict):
        super(singleton, cls).__init__(name, bases, dict)
        cls.instance = None

    def __call__(cls,*args,**kw):
        if cls.instance is None:
            cls.instance = super(singleton, cls).__call__(*args, **kw)
        return cls.instance
